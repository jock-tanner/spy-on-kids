# -*- coding: utf-8 -*-

from django.contrib import admin

from models import DispatchTable

class DispatchTableAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', 'phone', 'smsc', )

admin.site.register(DispatchTable, DispatchTableAdmin)
