# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DispatchTable'
        db.create_table(u'dispatcher_dispatchtable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(default=u'', max_length=12, blank=True)),
            ('phone', self.gf('django.db.models.fields.TextField')(default=u'dummy', blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
        ))
        db.send_create_signal(u'dispatcher', ['DispatchTable'])


    def backwards(self, orm):
        # Deleting model 'DispatchTable'
        db.delete_table(u'dispatcher_dispatchtable')


    models = {
        u'dispatcher.dispatchtable': {
            'Meta': {'ordering': "['order']", 'unique_together': '()', 'object_name': 'DispatchTable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '12', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'phone': ('django.db.models.fields.TextField', [], {'default': "u'dummy'", 'blank': 'True'})
        }
    }

    complete_apps = ['dispatcher']