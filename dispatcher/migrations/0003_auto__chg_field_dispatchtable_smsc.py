# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'DispatchTable.smsc'
        db.alter_column(u'dispatcher_dispatchtable', 'smsc', self.gf('django.db.models.fields.CharField')(max_length=12))

    def backwards(self, orm):

        # Changing field 'DispatchTable.smsc'
        db.alter_column(u'dispatcher_dispatchtable', 'smsc', self.gf('django.db.models.fields.TextField')())

    models = {
        u'dispatcher.dispatchtable': {
            'Meta': {'ordering': "['order']", 'unique_together': '()', 'object_name': 'DispatchTable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '12', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'phone': ('django.db.models.fields.TextField', [], {'default': "u'websms'", 'blank': 'True'}),
            'smsc': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '12', 'blank': 'True'})
        }
    }

    complete_apps = ['dispatcher']