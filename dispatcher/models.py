# -*- coding: utf-8 -*-

from django.db import models

class DispatchTable(models.Model):

    key = models.CharField(verbose_name=u'Ключ', max_length=12, blank=True, default=u'')
    phone = models.TextField(verbose_name=u'Имя целевого модема', blank=True, default=u'websms')
    smsc = models.CharField(verbose_name=u'SMS-центр', blank=True, default=u'', max_length=12)
    order = models.IntegerField(verbose_name=u'Порядок просмотра', blank=True, default=0)

    class Meta:
        ordering = ['order', ]
        verbose_name = u'Направление'
        verbose_name_plural = u'Таблица диспетчеризации SM'
