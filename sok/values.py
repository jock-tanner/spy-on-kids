# -*- coding: utf-8 -*-

import dbsettings

class ServerOptions(dbsettings.Group):
    smsdrc_template = dbsettings.StringValue(u'Шаблон поиска файлов конфигурации gammu-smsd')
    interface = dbsettings.StringValue(u'Сетевой интерфейс (eth0)', default='eth0')
    max_bindings = dbsettings.PositiveIntegerValue(u'Лимит подключений от одного пользователя',
                                                   default=2)

class ClientOptions(dbsettings.Group):
    xml_login = dbsettings.StringValue(u'Имя пользователя websms.ru')
    xml_password = dbsettings.PasswordValue(u'Пароль websms.ru')
    xml_sender = dbsettings.StringValue(u'Имя отправителя')
    xml_sleep = dbsettings.PositiveIntegerValue(u'Периодичность проверки статуса, с', default=900)
