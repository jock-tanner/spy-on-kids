# -*- coding: utf-8 -*-

import random, string, subprocess, requests, datetime, pytz

from django.conf import settings
from django.db.models import Max
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.models import get_current_site
from django.dispatch import receiver, Signal

from django_ajax.decorators import ajax

from smsd_storage.models import SentItems
from forms import PasswordResetForm
from values import ServerOptions, ClientOptions
from dispatcher.models import DispatchTable
from smsd_storage.models import SentItems
from lxml.builder import E
from lxml import etree

# show options
server_options = ServerOptions(u'Настройки SMPP-сервера')
client_options = ClientOptions(u'Настройки websms.ru')

send_password = Signal(providing_args=["username", "raw_password", ])

@ajax
def user_applet(request):
    # TODO: make templates for this
    if request.user.is_authenticated():
        data = {
            'inner-fragments': {
                '#user-applet': u'<p class="navbar-text">Здравствуйте, {1} {2}!</p>'
                u'<li><a href="{3}">Сообщения</a></li>'
                u'<li><a href="{0}">Выход</a></li>'.format(
                    reverse('logout'), request.user.first_name, request.user.last_name,
                    reverse('view_sms')),
                },
            }
    else:
        data = {
            'inner-fragments': {
                '#user-applet': u'<li><a href="{0}">Вход</a></li>'.format(reverse('login')),
                },
            }
    return data


# List of sent SMs
@login_required
def view_sms(request):
    # filter for telephone number
    sm_list = SentItems.objects.filter(destination_number=''.join(['+', request.user.username]))
    paginator = Paginator(sm_list, 15)
    page = request.GET.get('page')
    try:
        sms = paginator.page(page)
    except PageNotAnInteger:
        sms = paginator.page(1)
    except EmptyPage:
        sms = paginator.page(paginator.num_pages)
    return render(request, 'view_sms.html', {'sms': sms})

# displays or posts password reset form
def password_reset(request):
    length = 8
    chars = string.ascii_letters + string.digits + '!@#$%^&*()'

    if request.method == 'POST':
        # reset password and redirect
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            if User.objects.filter(username=form.cleaned_data['username']).count():
                # generate random password
                raw_password = ''.join(random.choice(chars) for i in range(length))
                # send user an SM with their new password
                send_password.send(sender=password_reset, username=form.cleaned_data['username'],
                                   raw_password=raw_password)
                # set password
                user = User.objects.get(username=form.cleaned_data['username'])
                user.set_password(raw_password)
                user.save()
            return redirect('password_reset_done')
    else:
        # render template
        form = PasswordResetForm()
        context = { 'form': form, }
        return TemplateResponse(request, 'registration/password_reset_form.html', context)


# send new password to user by SM
@receiver(send_password)
def proc_send_password(sender, **kwargs):
    XML_IFACE = 'http://cab.websms.ru/xml_in5.asp'

    dst_addr = kwargs['username']
    if dst_addr[0] == '+':
        dst_addr = dst_addr[1:]
    if not dst_addr.isdigit():
        return
    text = u'Ваш новый пароль: {0}'.format(kwargs['raw_password'])
    modem, smsc = get_modem(dst_addr)
    if modem is None:
        return
    if modem == 'websms':
        # send through websms.ru gateway (HTTP/XML)
        local_tz = pytz.timezone(settings.TIME_ZONE)
        # generate SM sequence number
        seq_no = str(random.randint(1, 2147483647))
        # create XML
        req = E.message(
            E.service(
                id = 'single',
                login = client_options.xml_login,
                password = client_options.xml_password,
                source = client_options.xml_sender,
                test = '0',
                uniq_key = seq_no),
            E.to(''.join(['+', dst_addr])),
            E.body(text))
        # post request and parse result
        resp = etree.XML(requests.post(XML_IFACE, data=etree.tostring(req, xml_declaration=True,
                                                        encoding='UTF-8')).text.encode('ascii'))
        resp_id = resp.get('id')                 # assigned by remote system
        resp_date = resp.get('date')
        resp_status = resp.find('state').text
        # save in database
        item = SentItems()
        item.sending_date_time = local_tz.localize(datetime.datetime.strptime(resp_date,
                                                                           '%d.%m.%Y %H:%M:%S'))
        item.destination_number = ''.join(['+', dst_addr])
        item.coding = 'Unicode_No_Compression'
        item.text_decoded = text
        item.sender_id = 'websms'
        item.sequence_position = 1
        if resp_status.upper() in ('ACCEPTED', 'SENT', 'DELIVERED', 'READY',
                                   'SPOOLED', 'ENROUTE', 'DELAYED'):
            item.status = 'SendingOK'
        else:
            item.status = 'Error'
        item.tpmr = int(resp_id)
        item.creator_id = 'SOK'
        # ID is not an AutoField!
        item.sent_items_id = SentItems.objects.all().aggregate(Max('sent_items_id'))['sent_items_id__max'] + 1
        item.save()
    else:
        # send through real modem
        inject = ['/usr/bin/gammu-smsd-inject',
                  '-c', server_options.smsdrc_template.format(modem),
                  'TEXT', ''.join(['+', dst_addr])]
        if len(smsc) > 2:
            if smsc[0] != '+':
                smsc = ''.join(['+', smsc])
            inject.extend(['-smscnumber', smsc])
        if is_unicode(text):
            inject.append('-unicode')
        inject.extend(['-text', text])
        # call gammu-smsd-inject
        subprocess.check_call(inject, shell=False)

# It's better to copypaste it than to plug in the whole smpp_server as a module

# get modem name from database using addressee's phone number as a key.
def get_modem(dst_addr):
    for direction in DispatchTable.objects.all():
        if dst_addr.startswith(direction.key):
            return [ direction.phone, direction.smsc ]
    return [ None, None ]


# is it necessary to encode the SM in utf-16?
def is_unicode(text):
    try:
        _ = text.encode('ascii')
    except (UnicodeEncodeError, UnicodeDecodeError):
        return True
    else:
        return False

