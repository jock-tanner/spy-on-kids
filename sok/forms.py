# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.hashers import UNUSABLE_PASSWORD
from django.contrib.auth.tokens import default_token_generator

class PasswordResetForm(forms.Form):
    error_messages = {
        'unknown': u'Пользователя с этим номером телефона не существует.',
        'unusable': u'Этому пользователю запрещено менять пароль.',
    }

    username = forms.CharField(label=u'Номер телефона', max_length=12)

    def clean_username(self):
        # Validates that an active user exists with the given phone number
        UserModel = get_user_model()
        username = self.cleaned_data['username']
        self.users_cache = UserModel._default_manager.filter(username=username)
        if not len(self.users_cache):
            raise forms.ValidationError(self.error_messages['unknown'])
        if not any(user.is_active for user in self.users_cache):
            # none of the filtered users are active
            raise forms.ValidationError(self.error_messages['unknown'])
        if any((user.password == UNUSABLE_PASSWORD)
               for user in self.users_cache):
            raise forms.ValidationError(self.error_messages['unusable'])
        return username

    # send SM with cleartext password
    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        pass


class AuthenticationForm(forms.Form):

    username = forms.CharField(max_length=12, label=u'Номер телефона')
    password = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': u'Номер телефона или пароль неверны.',
        'no_cookies': u'Ваш браузер не поддерживает куки',
        'inactive': u'Ваша учётная запись заблокирована',
    }

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'] % {
                        'username': username
                    })
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])
        self.check_for_test_cookie()
        return self.cleaned_data

    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(self.error_messages['no_cookies'])

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache
