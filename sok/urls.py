# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.template import RequestContext

from forms import PasswordResetForm, AuthenticationForm
from views import user_applet, view_sms, password_reset

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^settings/', include('dbsettings.urls')),
    url(r'^login/$', 'django.contrib.auth.views.login',
        { 'authentication_form': AuthenticationForm }, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
#    url(r'^reset/$', 'django.contrib.auth.views.password_reset',
#        { 'post_reset_redirect': '/reset/done/',
#          'password_reset_form': PasswordResetForm },
#        name='password_reset'),
    url(r'^reset/$', password_reset, name='password_reset'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_done',
        name='password_reset_done'),
    url(r'^user-applet/$', user_applet, name='user_applet'),
    url(r'^view/$', view_sms,  name='view_sms'),
)
