# -*- coding: utf-8 -*-

import os, site

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
site.addsitedir(BASE_DIR)
site.addsitedir(os.path.join(BASE_DIR, '../lib/python2.7/site-packages'))
activate_this = os.path.join(BASE_DIR, '../bin/activate_this.py')
execfile(activate_this, dict(__file__ = activate_this))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sok.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
