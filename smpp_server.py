# -*- coding: utf-8 -*-

import os, sys, socket, fcntl

# virtualenv
ROOT = os.path.dirname(os.path.realpath(__file__))
sys.path.append(ROOT)
activate_this = os.path.join(ROOT, '../bin/activate_this.py')
execfile(activate_this, dict(__file__ = activate_this))

import struct, time, datetime, logging, requests, random, pytz, subprocess

from multiprocessing import Process, Queue
from zope.interface import implements
from twisted.python import log, failure
from twisted.cred import portal, checkers, credentials, error
from twisted.spread import pb
from twisted.internet import defer, reactor

from smpp.twisted.server import SMPPServerFactory
from smpp.twisted.config import SMPPServerConfig
from smpp.pdu.sm_encoding import SMStringEncoder

from lxml.builder import E
from lxml import etree

####################################################################################################
#                                                                                                  #
#   Some prologue code                                                                             #
#                                                                                                  #
####################################################################################################

queue = None

# cyrillic console
if sys.getdefaultencoding() != 'utf-8':
    reload(sys)
    sys.setdefaultencoding('utf-8')

# plug in django config
sys.path.append(os.path.dirname(__file__))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sok.settings')

from django.conf import settings
from django.db.models import Max
from django.contrib.auth.models import User, check_password

from dispatcher.models import DispatchTable
from smsd_storage.models import SentItems

# make use of dbsettings
from sok.values import ServerOptions, ClientOptions

server_options = ServerOptions()
client_options = ClientOptions()

# TODO: where to store it?
SMSC = { 'Number': '+79147991000', }

# initialize parts storage

sm_parts = { }

####################################################################################################
#                                                                                                  #
#   Server event handlers.                                                                         #
#   Server loop must be run in the host multiprocessing process (main procedure)                   #
#                                                                                                  #
####################################################################################################

# handle incoming messages 
def server_msg_handler(system_id, smpp, pdu):
    log.msg('User {0} has sent a message.'.format(system_id))

    # get text and PDU
    sm_encoder = SMStringEncoder()
    sm_string = sm_encoder.decodeSM(pdu)
    dst_addr = pdu.params['destination_addr']

    if sm_string.udh is None:
        # one part message
        log.msg('Single message')
        dispatch_sm(system_id, dst_addr, sm_string.unicode)
    else:
        # expecting multipart message in UDH
        seq_num = sm_string.udh[0].data.sequenceNum
        max_num = sm_string.udh[0].data.maximumNum

        log.msg('Msg part #{0} from {1}'.format(seq_num, max_num))

        if seq_num < max_num:
            # store part
            log.msg('Storing {0}'.format(sm_string.unicode))
            sm_parts['_'.join([system_id, pdu.params['destination_addr'],
                               str(seq_num)])] = sm_string.unicode
        else:
            # reconstruct message from parts
            full_sm_text = u''
            i = 1
            while True:
                full_sm_text = u''.join([full_sm_text,
                                         sm_parts[u'_'.join([system_id,
                                                             pdu.params['destination_addr'],
                                                             unicode(i)])]])
                i += 1
                if i >= max_num:
                    break
            full_sm_text = u''.join([full_sm_text, sm_string.unicode])

            dispatch_sm(system_id, dst_addr, full_sm_text)

# send SM through modem or gateway
def dispatch_sm(system_id, dst_addr, text):
    global queue

    modem, smsc = get_modem(dst_addr)
    if modem is None:
        log.msg('Destination number {0} is ignored'.format(dst_addr))
        return
    if modem == 'websms':
        # send through websms.ru gateway (HTTP/XML)
        queue.put([text, dst_addr])
    else:
        # send through real modem
        inject = ['/usr/bin/gammu-smsd-inject',
                  '-c', server_options.smsdrc_template.format(modem),
                  'TEXT', ''.join(['+', dst_addr])]
        if len(smsc) > 2:
            if smsc[0] != '+':
                smsc = ''.join(['+', smsc])
            inject.extend(['-smscnumber', smsc])
        if is_unicode(text):
            inject.append('-unicode')
        inject.extend(['-text', u'{0}'.format(text)])
        # call gammu-smsd-inject
        subprocess.check_call(inject, shell=False)
    log.msg('Sent {0} to {1}'.format(text, modem))


####################################################################################################
#                                                                                                  #
#   Client procedure. Posting SMS through HTTP/XML                                                 #
#                                                                                                  #
####################################################################################################

XML_IFACE = 'http://cab.websms.ru/xml_in5.asp'

def xml_client(queue):
    local_tz = pytz.timezone(settings.TIME_ZONE)
    while True:
        try:
            # get command from queue
            text, dst_addr = queue.get()
            # generate SM sequence number
            seq_no = str(random.randint(1, 2147483647))
            log.msg('Relaying ({0}) to {1}...'.format(text, dst_addr))
            # create XML
            req = E.message(
                E.service(
                    id = 'single',
                    login = client_options.xml_login,
                    password = client_options.xml_password,
                    source = client_options.xml_sender,
                    test = '0',
                    uniq_key = seq_no),
                E.to(''.join(['+', dst_addr])),
                E.body(text))
            # post request and parse result
            resp = etree.XML(requests.post(XML_IFACE, data=etree.tostring(req, xml_declaration=True,
                                                        encoding='UTF-8')).text.encode('ascii'))
            resp_id = resp.get('id')                 # assigned by remote system
            resp_date = resp.get('date')
            resp_status = resp.find('state').text
            log.msg('Relaying done, id: {0}, sequence no: {1}, '
                    'timestamp: {2}, status: {3}'.format(resp_id, seq_no, resp_date, resp_status))
            # save in database
            item = SentItems()
            item.sending_date_time = pytz.utc.localize(datetime.datetime.strptime(resp_date,
                        '%d.%m.%Y %H:%M:%S') + datetime.timedelta(hours=7)).astimezone(local_tz)
            item.destination_number = ''.join(['+', dst_addr])
            item.coding = 'Unicode_No_Compression'
            item.text_decoded = text
            item.sender_id = 'websms'
            item.sequence_position = 1
            if resp_status.upper() in ('ACCEPTED', 'SENT', 'DELIVERED', 'READY',
                                                                'SPOOLED', 'ENROUTE', 'DELAYED'):
                item.status = 'SendingOK'
            else:
                item.status = 'Error'
            item.tpmr = int(resp_id)
            item.creator_id = 'SOK'
            # ID is not an AutoField!
            item.sent_items_id = SentItems.objects.all().aggregate(Max('sent_items_id'))['sent_items_id__max'] + 1
            item.save()
            log.msg('Saved in SentItems')
        except Exception as e:
            log.msg('Caught {0}\nBetter luck next time!'.format(e))

def xml_checker():
    local_tz = pytz.timezone(settings.TIME_ZONE)
    # create XML
    req = E.request(
        'status',                         # XML tag content
        id = '',                          # message system ID (TPMR)
        login = client_options.xml_login,
        password = client_options.xml_password)
    while True:                           # main loop
        log.msg('Checking relayed SMs statuses...')
        # get recorded SMs
        sms = SentItems.objects.filter(sender_id='websms', status='SendingOK')
        # check statuses
        for sm in sms:
            log.msg('Checking {0}'.format(sm.sent_items_id))
            req.set('id', unicode(sm.tpmr))
            # post request and parse result
            resp = etree.XML(requests.post(XML_IFACE, data=etree.tostring(req, xml_declaration=True,
                                                        encoding='UTF-8')).text.encode('ascii'))
            resp_status = resp.find('state').text
            log.msg('Status: {0}'.format(resp_status))
            resp_status = resp_status.upper()
            if resp_status == 'DELIVERED':
                sm.status = 'DeliveryOK'
            elif resp_status in ('REJECTED', 'UNDELIVERABLE', 'DELETED', 'EXPIRED', 'RESTRICTED',
                                 'UNROUTABLE'):
                sm.status = 'DeliveryFailed'
            elif resp_status in ('UNKNOWN', 'NOT FOUND', 'NOT_FOUND'):
                sm.status = 'DeliveryUnknown'
            # other statuses intentionally ignored
            if sm.insert_into_db:
                sm.insert_into_db = pytz.utc.localize(sm.insert_into_db).astimezone(local_tz)
            if sm.sending_date_time:
                sm.sending_date_time = pytz.utc.localize(sm.sending_date_time).astimezone(local_tz)
            sm.save()

        time.sleep(client_options.xml_sleep)


####################################################################################################
#                                                                                                  #
# Server authentication code mostly taken from                                                     #
# www.robgolding.com/blog/2010/11/10/using-django-authentication-with-twisted-perspective-broker/  #
#                                                                                                  #
####################################################################################################

class DjangoAuthChecker:
    implements(checkers.ICredentialsChecker)
    credentialInterfaces = (credentials.IUsernamePassword, )

    def _passwordMatch(self, matched, user):
        if matched:
            return user
        else:
            return failure.Failure(error.UnauthorizedLogin())

    def requestAvatarId(self, credentials):
        try:
            user = User.objects.get(username=credentials.username, is_staff=True)
            return defer.maybeDeferred(
                check_password,
                credentials.password,
                user.password).addCallback(self._passwordMatch, user)
        except User.DoesNotExist:
            return defer.fail(error.UnauthorizedLogin())

class SMPPRealm(object):
    implements(portal.IRealm)

    def requestAvatar(self, user, mind, *interfaces):
        #assert pb.IPerspective in interfaces
        avatar = SMPPAvatar(user)
        avatar.attached(mind)
        return pb.IPerspective, avatar, lambda a=avatar:a.detached(mind)

class SMPPAvatar(pb.Avatar):
    def __init__(self, user):
        self.user = user

    def attached(self, mind):
        self.remote = mind
        log.msg('User {0} connected'.format(self.user))

    def detached(self, mind):
        self.remote = None
        log.msg('User {0} disconnected'.format(self.user))


####################################################################################################
#                                                                                                  #
# Main procedure should only launch server and client in separate processes                        #
# and share Queue object between them                                                              #
#                                                                                                  #
####################################################################################################

def main(argv):
    global queue

    # turn on logging
    logging.basicConfig()
    log.startLogging(sys.stdout)

    # server reactor parameters
    # TODO: systems should be stored alongside the user info
    systems = {
        'tanner': {
            'max_bindings': 2,
        },
        'yuri': {
            'max_bindings': 2,
        },
    }
    server_config = SMPPServerConfig(systems=systems, msgHandler=server_msg_handler)

    # auth settings
    checker = DjangoAuthChecker()
    realm = SMPPRealm()
    auth_portal = portal.Portal(realm, [checker])

    # standard SMPP port is 2775
    reactor.listenTCP(2222, SMPPServerFactory(server_config, auth_portal),
                      interface=get_ip_address(server_options.interface))
    # initialize queue
    queue = Queue()

    # start HTTP/XML websms.ru client
    client_proc = Process(target=xml_client, args=(queue, ))
    client_proc.start()

    checker_proc = Process(target=xml_checker)
    checker_proc.start()

    reactor.run()
    return 0


####################################################################################################
#                                                                                                  #
#   Some common and standalone procedures                                                          #
#                                                                                                  #
####################################################################################################

# get modem name from database using addressee's phone number as a key
def get_modem(dst_addr):
    for direction in DispatchTable.objects.all():
        if dst_addr.startswith(direction.key):
            return [ direction.phone, direction.smsc ]
    return [ None, None ]


# got it from stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-of-eth0-in-python
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


# is it necessary to encode the SM in utf-16?
def is_unicode(text):
    try:
        _ = text.encode('ascii')
    except (UnicodeEncodeError, UnicodeDecodeError):
        return True
    else:
        return False


# main() centinel
if __name__ == '__main__':
    sys.exit(main(sys.argv))

