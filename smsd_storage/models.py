# -*- coding: utf-8 -*-

#from __future__ import unicode_literals

from django.db import models
from compositekey import db

SMSD_BOOL = (
    ('false', u'Нет'),
    ('true', u'Да'),
)

SMSD_YESNO = (
    ('yes', u'Да'),
    ('no', u'Нет'),
)

SMSD_TRISTATE = (
    ('default', 'Default'),
    ('yes', u'Да'),
    ('no', u'Нет'),
)

SMSD_CODING = (
    ('Default_No_Compression', 'Default_No_Compression'),
    ('Unicode_No_Compression', 'Unicode_No_Compression'),
    ('8bit', '8bit'),
    ('Default_Compression', 'Default_Compression'),
    ('Unicode_Compression', 'Unicode_Compression'),
)

SMSD_STATUS = (
    ('SendingOK', 'Sending OK'),
    ('SendingOKNoReport', 'Sending OK (No Report)'),
    ('SendingError', 'Sending Error'),
    ('DeliveryOK', 'Delivery OK'),
    ('DeliveryFailed', 'Delivery Failed'),
    ('DeliveryPending', 'Delivery Pending'),
    ('DeliveryUnknown', 'Delivery Unknown'),
    ('Error', 'Error'),
)

class Daemons(models.Model):
    start = models.TextField(db_column='Start')
    info = models.TextField(db_column='Info')

    class Meta:
        managed = False
        db_table = 'daemons'


class Gammu(models.Model):
    version = models.IntegerField(db_column='Version')

    class Meta:
        managed = False
        db_table = 'gammu'


class Inbox(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    updated_in_db = models.DateTimeField(db_column='UpdatedInDB', auto_now=True, auto_now_add=True)
    receiving_date_time = models.DateTimeField(db_column='ReceivingDateTime')
    text = models.TextField(db_column='Text')
    sender_number = models.CharField(db_column='SenderNumber', max_length=20)
    coding = models.CharField(db_column='Coding', max_length=22,
                              choices=SMSD_CODING, default='Default_No_Compression')
    udh = models.TextField(db_column='UDH')
    smsc_number = models.CharField(db_column='SMSCNumber', max_length=20)
    class_field = models.IntegerField(db_column='Class')
    text_decoded = models.TextField(db_column='TextDecoded')
    recipient_id = models.TextField(db_column='RecipientID')
    processed = models.CharField(db_column='Processed', max_length=5,
                                 choices=SMSD_BOOL, default='false')

    class Meta:
        managed = False
        db_table = 'inbox'


class Outbox(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    updated_in_db = models.DateTimeField(db_column='UpdatedInDB', auto_now=True)
    insert_into_db = models.DateTimeField(db_column='InsertIntoDB', auto_now_add=True)
    sending_date_time = models.DateTimeField(db_column='SendingDateTime')
    send_before = models.TimeField(db_column='SendBefore')
    send_after = models.TimeField(db_column='SendAfter')
    text = models.TextField(db_column='Text', blank=True)
    destination_number = models.CharField(db_column='DestinationNumber', max_length=20)
    coding = models.CharField(db_column='Coding', max_length=22,
                              choices=SMSD_CODING, default='Default_No_Compression')
    udh = models.TextField(db_column='UDH', blank=True)
    class_field = models.IntegerField(db_column='Class', blank=True, null=True)
    text_decoded = models.TextField(db_column='TextDecoded')
    multipart = models.CharField(db_column='MultiPart', max_length=5,
                                 choices=SMSD_BOOL, default='false')
    relative_validity = models.IntegerField(db_column='RelativeValidity', blank=True, null=True)
    sender_id = models.CharField(db_column='SenderID', max_length=255, blank=True)
    sending_timeout = models.DateTimeField(db_column='SendingTimeOut', blank=True, null=True)
    delivery_report = models.CharField(db_column='DeliveryReport', max_length=7,
                                       choices=SMSD_TRISTATE, default='default')
    creator_id = models.TextField(db_column='CreatorID')

    class Meta:
        managed = False
        db_table = 'outbox'


class OutboxMultipart(models.Model):
    id = id = db.MultiFieldPK('outbox_multipart_id', 'sequence_position')
    outbox_multipart_id = models.IntegerField(db_column='ID')
    text = models.TextField(db_column='Text', blank=True)
    coding = models.CharField(db_column='Coding', max_length=22,
                              choices=SMSD_CODING, default='Default_No_Compression')
    udh = models.TextField(db_column='UDH', blank=True)
    class_field = models.IntegerField(db_column='Class', blank=True, null=True, default=-1)
    text_decoded = models.TextField(db_column='TextDecoded', blank=True, null=True)
    sequence_position = models.IntegerField(db_column='SequencePosition', default=1)

    class Meta:
        managed = False
        db_table = 'outbox_multipart'


class PBK(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    group_id = models.IntegerField(db_column='GroupID', default=-1)
    name = models.TextField(db_column='Name')
    number = models.TextField(db_column='Number')

    class Meta:
        managed = False
        db_table = 'pbk'


class PBKGroups(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    name = models.TextField(db_column='Name')

    class Meta:
        managed = False
        db_table = 'pbk_groups'


class Phones(models.Model):
    id = models.TextField(db_column='ID')
    updated_in_db = models.DateTimeField(db_column='UpdatedInDB', auto_now=True, auto_now_add=True)
    insert_into_db = models.DateTimeField(db_column='InsertIntoDB')
    timeout = models.DateTimeField(db_column='TimeOut')
    send = models.CharField(db_column='Send', max_length=3,
                            choices=SMSD_YESNO, default='no')
    receive = models.CharField(db_column='Receive', max_length=3,
                               choices=SMSD_YESNO, default='no')
    imei = models.CharField(db_column='IMEI', primary_key=True, max_length=35)
    net_code = models.CharField(db_column='NetCode', max_length=10, default='ERROR')
    net_name = models.CharField(db_column='NetName', max_length=35, default='ERROR')
    client = models.TextField(db_column='Client')
    battery = models.IntegerField(db_column='Battery', default=-1)
    signal = models.IntegerField(db_column='Signal', default=-1)
    sent = models.IntegerField(db_column='Sent', default=0)
    received = models.IntegerField(db_column='Received', default=0)

    class Meta:
        managed = False
        db_table = 'phones'


class SentItems(models.Model):
    id = db.MultiFieldPK('sent_items_id', 'sequence_position')
    updated_in_db = models.DateTimeField(db_column='UpdatedInDB', auto_now_add=True, auto_now=True)
    insert_into_db = models.DateTimeField(db_column='InsertIntoDB')
    sending_date_time = models.DateTimeField(db_column='SendingDateTime')
    delivery_date_time = models.DateTimeField(db_column='DeliveryDateTime', blank=True, null=True)
    text = models.TextField(db_column='Text')
    destination_number = models.CharField(db_column='DestinationNumber', max_length=20, default='')
    coding = models.CharField(db_column='Coding', max_length=22,
                              choices=SMSD_CODING, default='Default_No_Compression')
    udh = models.TextField(db_column='UDH')
    smsc_number = models.CharField(db_column='SMSCNumber', max_length=20, default='')
    class_field = models.IntegerField(db_column='Class', default=-1)
    text_decoded = models.TextField(db_column='TextDecoded', default='')
    sent_items_id = models.IntegerField(db_column='ID', default=0)
    sender_id = models.CharField(db_column='SenderID', max_length=255)
    sequence_position = models.IntegerField(db_column='SequencePosition', default=-1)
    status = models.CharField(db_column='Status', max_length=17,
                              choices=SMSD_STATUS, default='SendingOK')
    status_error = models.IntegerField(db_column='StatusError', default=-1)
    tpmr = models.IntegerField(db_column='TPMR', default=-1)
    relative_validity = models.IntegerField(db_column='RelativeValidity', default=-1)
    creator_id = models.TextField(db_column='CreatorID')

    class Meta:
        managed = False
        db_table = 'sentitems'
        ordering = ['-sending_date_time', ]

