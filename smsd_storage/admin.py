# -*- coding: utf-8 -*-

from django.contrib import admin

from models import Inbox, Outbox, OutboxMultipart, PBK, PBKGroups, Phones, SentItems

class InboxAdmin(admin.ModelAdmin):
    pass

class OutboxAdmin(admin.ModelAdmin):
    pass

class OutboxMultipartAdmin(admin.ModelAdmin):
    pass

class PBKAdmin(admin.ModelAdmin):
    pass

class PBKGroupsAdmin(admin.ModelAdmin):
    pass

class PhonesAdmin(admin.ModelAdmin):
    pass

class SentItemsAdmin(admin.ModelAdmin):
    pass

admin.site.register(Inbox, InboxAdmin)
admin.site.register(Outbox, OutboxAdmin)
admin.site.register(OutboxMultipart, OutboxMultipartAdmin)
admin.site.register(PBK, PBKAdmin)
admin.site.register(PBKGroups, PBKGroupsAdmin)
admin.site.register(Phones, PhonesAdmin)
admin.site.register(SentItems, SentItemsAdmin)
